/* #![feature(nll)] */
extern crate itertools;
extern crate snmp;
use std::net;

pub mod cablemodem;
pub mod mibs;

#[macro_use]
extern crate log;
extern crate env_logger;
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct MacAddress {
    bytes: [u8; 6],
}

impl MacAddress {
    /// Creates a new `MacAddress` struct from the given bytes.
    pub fn new(bytes: [u8; 6]) -> MacAddress {
        MacAddress { bytes }
    }
}

impl std::fmt::Display for MacAddress {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let _ = write!(
            f,
            "{:<02X}:{:<02X}:{:<02X}:{:<02X}:{:<02X}:{:<02X}",
            self.bytes[0],
            self.bytes[1],
            self.bytes[2],
            self.bytes[3],
            self.bytes[4],
            self.bytes[5]
        );

        Ok(())
    }
}
#[derive(Debug)]
pub struct LANDevice {
    pub id: u32,
    pub mac: Option<MacAddress>,
    pub ip: Option<net::IpAddr>,
    pub hostname: Option<String>,
}

impl LANDevice {
    pub fn new(
        id: u32,
        mac: Option<MacAddress>,
        ip: Option<net::IpAddr>,
        hostname: Option<&str>,
    ) -> LANDevice {
        let hname = match hostname {
            Some(h) => Some(String::from(h)),
            None => None,
        };
        LANDevice {
            id: id,
            mac: mac,
            ip: ip,
            hostname: hname,
        }
    }
}
