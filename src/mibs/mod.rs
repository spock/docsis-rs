use std::fmt;

pub static CABLE_MODEM_UPGRADE_SERVER: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 3, 1, 0];
pub static CABLE_MODEM_UPGRADE_FILENAME: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 3, 2, 0];
pub static CABLE_MODEM_UPGRADE_STATUS: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 3, 3, 0];
pub static CABLE_MODEM_SOFTWARE_VERSION: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 3, 5, 0];
pub static CABLE_MODEM_VERSION: &'static [u32] = &[1, 3, 6, 1, 2, 1, 1, 1, 0];
pub static CABLE_MODEM_LOGS: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 5, 8, 1, 7];
pub static CABLE_MODEM_RESET: &'static [u32] = &[1, 3, 6, 1, 2, 1, 69, 1, 1, 3, 0];
// DOWNSTREAM_DS_LIST contans table of active downstreams
pub static CABLE_MODEM_DOWNSTREAM_LIST: &'static [u32] =
    &[1, 3, 6, 1, 2, 1, 10, 127, 1, 1, 1, 1, 6];
pub static CABE_MODEM_UPSTREAM_LIST: &'static [u32] = &[1, 3, 6, 1, 2, 1, 10, 127, 1, 2, 2, 1, 3];

pub mod tc7200 {
    pub static CG_UI_ADVANCED_FORWARDING_PORT_START_VALUE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 2];
    pub static CG_UI_ADVANCED_FORWARDING_PORT_END_VALUE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 3];
    pub static CG_UI_ADVANCED_FORWARDING_PROTOCOL_TYPE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 4];
    pub static CG_UI_ADVANCED_FORWARDING_IPADDR_TYPEE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 5];
    pub static CG_UI_ADVANCED_FORWARDING_IPADDR: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 6];
    pub static CG_UI_ADVANCED_FORWARDING_ENABLED: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 7];
    pub static CG_UI_ADVANCED_FORWARDING_ROW_STATUS: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 8];
    pub static CG_UI_ADVANCED_FORWARDING_PORT_INTERNAL_START_VALUE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 9];
    pub static CG_UI_ADVANCED_FORWARDING_PPORT_INTERNAL_END_VALUE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 1];
    pub static CG_UI_ADVANCED_FORWARDING_REMOTE_IPADDR: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 1];
    pub static CG_UI_ADVANCED_FORWARDING_DESCRIPTION: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 1];
    pub static CG_UI_ADVANCED_FORWARDING_REMOVE: &'static [u32] =
        &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 33, 2, 5, 1, 13];
    pub static LAN_ADDR_TABLE: &'static [u32] = &[1, 3, 6, 1, 4, 1, 2863, 205, 10, 1, 13];
}
#[allow(non_upper_case_globals)]
pub mod cga2121 {
    pub static LAN_ADDR_TABLE: &'static [u32] = &[1, 3, 6, 1, 4, 1, 46366, 4292, 79, 2, 3, 4, 1];

    pub enum LanAddrTableRow {
        _0,
        _1,
        _2,
        IpAddr,     // Device IP address
        PhysAddr,   // Device Physical Address (MAC)
        CreateTime, // Lease creation time
        ExpireTime, // Lease expire time
        HostName,   // Device hostname reported by client
        ClientId,
        Interface,
        ClientNotes,
        CpeStatus,
        SignalStrength,
        OptionList,
        RequestList,
    }

}

// SNMP values mapping
pub enum CableModemStatus {
    _1,
    _2,
}
#[derive(Debug)]
pub enum IPAddrType {
    Unknown,
    IPv4,
}

impl fmt::Display for IPAddrType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            IPAddrType::Unknown => write!(f, "{}", "Unknown"),
            IPAddrType::IPv4 => write!(f, "{}", "IPv4"),
        }
    }
}
