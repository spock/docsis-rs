#[macro_use]
extern crate clap;
extern crate docsis_rs;
use std::net;
extern crate env_logger;
extern crate log;
// use docsis::mibs;
//const CFG_NAME: &'static str = "docsis.conf";
const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const APP_NAME: &'static str = env!("CARGO_PKG_NAME");
const APP_AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");

fn main() {
    env_logger::init();
    let app = clap::App::new(APP_NAME)
        .version(VERSION)
        .author(APP_AUTHOR)
        .subcommand(
            clap::SubCommand::with_name("restart")
                .about("Restart cable modem")
                .arg(
                    clap::Arg::with_name("community")
                        .takes_value(true)
                        .short("c")
                        .help("SNMP community")
                        .default_value("private"),
                )
                .arg(
                    clap::Arg::with_name("port")
                        .takes_value(true)
                        .short("p")
                        .help("SNMP port")
                        .default_value("161"),
                )
                .arg(clap::Arg::with_name("ipaddress").required(true).index(1)),
        )
        .subcommand(
            clap::SubCommand::with_name("info")
                .about("Get information from cable modem")
                .arg(
                    clap::Arg::with_name("community")
                        .takes_value(true)
                        .short("c")
                        .help("SNMP community")
                        .default_value("private"),
                )
                .arg(
                    clap::Arg::with_name("port")
                        .takes_value(true)
                        .short("p")
                        .help("SNMP port")
                        .default_value("161"),
                )
                .arg(clap::Arg::with_name("ipaddress").required(true).index(1)),
        )
        .subcommand(
            clap::SubCommand::with_name("devlist")
                .about("Get list of devices connected to cable modem")
                .arg(
                    clap::Arg::with_name("community")
                        .takes_value(true)
                        .short("c")
                        .help("SNMP community")
                        .default_value("private"),
                )
                .arg(
                    clap::Arg::with_name("port")
                        .takes_value(true)
                        .short("p")
                        .help("SNMP port")
                        .default_value("161"),
                )
                .arg(clap::Arg::with_name("ipaddress").required(true).index(1)),
        )
        .get_matches();
    match app.subcommand() {
        ("cmrestart", Some(args)) => cm_restart(args),
        ("info", Some(args)) => cm_info(args),
        ("devlist", Some(args)) => device_list(args),
        (&_, None) | (&_, Some(_)) => {
            println!(
                "Missing command. {} --help to see available options",
                APP_NAME
            );
        }
    }
}
fn cm_restart(app: &clap::ArgMatches) {
    //env_logger::init().unwrap();
    let community = app.value_of("community").unwrap(); // unwrap() is safe since there is default value
    let ipaddress = app.value_of("ipaddress").unwrap(); // unwrap() is safe since there is default value
    let snmp_port = value_t!(app.value_of("port"), u16).unwrap_or_else(|e| e.exit());

    let result = docsis_rs::cablemodem::restart_cable_modem(
        &format!("{}:{}", ipaddress, snmp_port),
        community.as_bytes(),
        2,
    );
    match result {
        Ok(()) => println!("OK: Modem is restarting"),
        Err(e) => println!("ERR: {:?}", e),
    }
}
fn cm_info(app: &clap::ArgMatches) {
    //env_logger::init().unwrap();
    let community = app.value_of("community").unwrap(); // unwrap() is safe since there is default value
    let ipaddress = app.value_of("ipaddress").unwrap(); // unwrap() is safe since there is default value
    let snmp_port = value_t!(app.value_of("port"), u16).unwrap_or_else(|e| e.exit());

    let result = docsis_rs::cablemodem::get_info(
        &format!("{}:{}", ipaddress, snmp_port),
        community.as_bytes(),
        2,
    );
    match result {
        Ok(sysdescr) => println!("OK:{}", sysdescr),
        Err(e) => println!("ERR: {:?}", e),
    }
}
fn device_list(app: &clap::ArgMatches) {
    let community = app.value_of("community").unwrap(); // unwrap() is safe since there is default value
    let ipaddress = app.value_of("ipaddress").unwrap(); // unwrap() is safe since there is default value
    let snmp_port = value_t!(app.value_of("port"), u16).unwrap_or_else(|e| e.exit());
    match docsis_rs::cablemodem::get_connected_devices_list(
        &format!("{}:{}", ipaddress, snmp_port),
        community.as_bytes(),
        2,
    ) {
        Ok(v) => {
            //println!("Got response:");
            for e in v.iter() {
                let hname = e.hostname.clone().unwrap_or(String::from(""));
                let ip = e
                    .ip
                    .clone()
                    .unwrap_or(net::IpAddr::V4(<net::Ipv4Addr>::new(0, 0, 0, 0)));
                println!("IP: {}, MAC: {}, Hostname: {}", ip, e.mac.unwrap(), hname);
            }
        }
        Err(e) => println!("Error get_connected_devices_list(): {:?}", e),
    }
}
