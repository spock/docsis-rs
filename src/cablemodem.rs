// module containing all funtctions related to cable modems
// none of this function will make any connection to CMTS
use super::mibs;
use itertools::free::join;
use net;
use snmp::{SnmpError, SyncSession, Value};
use std::collections::HashMap;
use std::net::IpAddr;
use std::time::Duration;

pub struct ConnectedDevices {
    pub ipaddr: IpAddr,
    pub mac: super::MacAddress,
    pub hostname: String,
}
// restart_cable_modem will restart cable modem via snmp
// destination is in format ip:port
pub fn restart_cable_modem(
    destination: &str,
    community: &[u8],
    timeout: u64,
) -> Result<(), SnmpError> {
    let mut session = SyncSession::new(
        destination,
        community,
        Some(Duration::from_secs(timeout)),
        0,
    ).unwrap();
    let response = session.set(&[(mibs::CABLE_MODEM_RESET, Value::Integer(1))]);
    match response {
        Ok(_) => Ok(()),
        Err(e) => Err(e),
    }
}

pub fn get_info(destination: &str, community: &[u8], timeout: u64) -> Result<String, SnmpError> {
    let mut session = SyncSession::new(
        destination,
        community,
        Some(Duration::from_secs(timeout)),
        0,
    ).unwrap();
    let response = session.get(mibs::CABLE_MODEM_VERSION);
    match response {
        Ok(mut vb) => {
            if let Some((_oid, Value::OctetString(sys_descr))) = vb.varbinds.next() {
                Ok(String::from_utf8_lossy(sys_descr).to_string())
            } else {
                Err(SnmpError::ValueOutOfRange)
            }
        }
        Err(e) => Err(e),
    }
}

// pub fn get_connected_devices_list(
//     destination: &str,
//     community: &[u8],
//     timeout: u64,
// ) -> Result<Vec<ConnectedDevices>, SnmpError> {
//     // lets get modem info
//     let cm_info = get_info(destination, community, timeout)?;
//     if cm_info.contains("CGA2121") {
//         println!("Modem is CGA2121");
//     }
//     if cm_info.contains("TC7200") {
//         println!("Modem is TC7200");
//     }
//     let mut session = SyncSession::new(
//         destination,
//         community,
//         Some(Duration::from_secs(timeout)),
//         0,
//     ).unwrap();
//     let mut done = false;
//     let mut query_oid = mibs::cga2121::LAN_ADDR_TABLE.to_vec();
//     debug!("Entering walkall()");
//     let result = session.walkall(&query_oid);
//     debug!("Exiting walkall()");
//     println!("Result: {:?}", result);
//     Err(SnmpError::AsnEof)
// }
pub fn get_connected_devices_list(
    destination: &str,
    community: &[u8],
    timeout: u64,
) -> Result<Vec<super::LANDevice>, SnmpError> {
    // lets get modem info
    let cm_info = get_info(destination, community, timeout)?;
    let mut done = false;
    //let mut query_oid: Vec<u32> = Vec::new();
    // mibs::cga2121::LAN_ADDR_TABLE.to_vec();
    //let result = session.getnext(query_oid);
    let mut out: HashMap<u32, super::LANDevice> = HashMap::new();
    let mut session = SyncSession::new(
        destination,
        community,
        Some(Duration::from_secs(timeout)),
        0,
    ).unwrap();
    //TODO: zamienić to na match {} i w opcji _ dać pusty &[u8]
    // wówczas łatwo będzie dodawać kolejne modemy bez duplikowania kodu
    if cm_info.contains("CGA2121") {
        info!("Modem is CGA2121");
        let mut query_oid = mibs::cga2121::LAN_ADDR_TABLE.to_vec();
        while !done {
            // unknown modem version - [].len() = 0
            if query_oid.len() == 0 {
                //done = true;
                break;
            }
            let result = session.getnext(&query_oid.clone());
            match result {
                Ok(vb) => {
                    for (_oid, _name) in vb.varbinds {
                        let table_prefix_len = query_oid.len();
                        let roid = format!("{}", _oid);
                        let qoid = join(query_oid.clone(), ".");
                        let _current_oid: Vec<u32> =
                            qoid.split(".").map(|x| x.parse::<u32>().unwrap()).collect();
                        let _nextoid: Vec<u32> =
                            roid.split(".").map(|x| x.parse::<u32>().unwrap()).collect();
                        // returned getnext this is wrong modem - break
                        if table_prefix_len > _nextoid.len() {
                            done = true;
                            break;
                        }
                        let _table_oid = _nextoid[table_prefix_len..].to_vec();
                        query_oid = _nextoid.clone();
                        if roid.contains(&qoid) {
                            //println!("Idziemy dalej :)..");
                            //println!("QOID: {:?}", qoid);
                            if let Value::OctetString(raw) = _name {
                                match _table_oid[0] {
                                    7 => {
                                        let id = _table_oid.last().unwrap().clone();
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.hostname = Some(format!(
                                                    "{}",
                                                    String::from_utf8_lossy(raw)
                                                ));
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        None,
                                                        None,
                                                        Some(&format!(
                                                            "{}",
                                                            String::from_utf8_lossy(raw)
                                                        )),
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                    8 => {
                                        let id = _table_oid.last().unwrap().clone();
                                        //println!("Found ID: {}", id);
                                        if raw.len() < 6 {
                                            break;
                                        }
                                        let mac: [u8; 6] =
                                            [raw[0], raw[1], raw[2], raw[3], raw[4], raw[5]];
                                        let mac_addr = super::MacAddress::new(mac);
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.mac = Some(mac_addr);
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        Some(mac_addr),
                                                        None,
                                                        None,
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                    4 | 5 | 6 => {
                                        // println!("ROID:{:?} MAC: {:x?}", _table_oid, raw);
                                    }
                                    _ => {
                                        // println!(
                                        //     "ROID:{:?} VALUE(UNKN): {}",
                                        //     _table_oid,
                                        //     String::from_utf8_lossy(raw)
                                        // );
                                    }
                                }
                            }
                            if let Value::IpAddress(raw) = _name {
                                match _table_oid[0] {
                                    3 => {
                                        let id = _table_oid.last().unwrap().clone();
                                        //println!("Found ID: {}", id);
                                        let ip_addr = net::IpAddr::from(raw);
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.ip = Some(ip_addr);
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        None,
                                                        Some(ip_addr),
                                                        None,
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                                //println!("ROID:{:?} VALUE: {:x?}", _table_oid, _name);
                            }
                        //println!("Next OID: {:?}", _nextoid);
                        //done = true;
                        } else {
                            done = true;
                        }
                    }
                    //Err(SnmpError::UnsupportedVersion)
                }
                //Err(e) => Err(e),
                Err(_) => {}
            }
        }
    }
    if cm_info.contains("TC7200") {
        info!("Modem is TC7200");
        let mut query_oid = mibs::tc7200::LAN_ADDR_TABLE.to_vec();
        // loop_id used only on development/debugging
        let mut loop_id: u32 = 0;
        while !done {
            loop_id += 1;
            debug!("Loop iteration: {}", loop_id);
            // unknown modem version - [].len() = 0
            if query_oid.len() == 0 {
                //done = true;
                break;
            }
            let result = session.getnext(&query_oid.clone());
            match result {
                Ok(vb) => {
                    for (_oid, _name) in vb.varbinds {
                        let table_prefix_len = query_oid.len();
                        let roid = format!("{}", _oid);
                        let qoid = join(query_oid.clone(), ".");
                        let _current_oid: Vec<u32> =
                            qoid.split(".").map(|x| x.parse::<u32>().unwrap()).collect();
                        let _nextoid: Vec<u32> =
                            roid.split(".").map(|x| x.parse::<u32>().unwrap()).collect();
                        // println!(
                        //     "Loop No: {}, QOID: {:?}, NOID: {:?}",
                        //     loop_id, _current_oid, _nextoid
                        // );
                        // returned getnext this is wrong modem - break
                        if table_prefix_len > _nextoid.len() {
                            done = true;
                            break;
                        }
                        let _table_oid =
                            _nextoid[super::mibs::tc7200::LAN_ADDR_TABLE.to_vec().len()..].to_vec();
                        query_oid = _nextoid.clone();
                        // println!("Loop No: {}, OID: {:?}, NAME:{:?}", loop_id, qoid, _name);
                        // println!("Loop No: {}. ROID: {:?}", loop_id, roid);

                        //if roid.contains(&qoid) {
                        if roid.contains(&join(super::mibs::tc7200::LAN_ADDR_TABLE.to_vec(), ".")) {
                            //println!("Idziemy dalej w pętli nr {} :)..", loop_id);
                            //println!("QOID: {:?}", qoid);
                            // second from last is table column index (ie. IP, MAC, Hostname)
                            // we later match on this
                            let tree_index = _table_oid.len()-2;
                            if let Value::OctetString(raw) = _name {
                                //println!("Loop No: {}, table_oid: {:?}, Tree Index: {}", loop_id, _table_oid, tree_index);
                                match _table_oid[tree_index] {
                                    3 => {
                                        //println!("Last: {:?}", _table_oid.last());
                                        let id = _table_oid.last().unwrap().clone();
                                        //println!("ID(3): {}", id);
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.hostname = Some(format!(
                                                    "{}",
                                                    String::from_utf8_lossy(raw)
                                                ));
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        None,
                                                        None,
                                                        Some(&format!(
                                                            "{}",
                                                            String::from_utf8_lossy(raw)
                                                        )),
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                    2 => {
                                        let id = _table_oid.last().unwrap().clone();
                                        //println!("ID(2): {}", id);
                                        if raw.len() < 6 {
                                            break;
                                        }
                                        let mac: [u8; 6] =
                                            [raw[0], raw[1], raw[2], raw[3], raw[4], raw[5]];
                                        let mac_addr = super::MacAddress::new(mac);
                                        //println!("MAC: {}", mac_addr);
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.mac = Some(mac_addr);
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        Some(mac_addr),
                                                        None,
                                                        None,
                                                    ),
                                                );
                                            }
                                        }
                                    }
                                    4 => {
                                        // got IP Address
                                        let id = _table_oid.last().unwrap().clone();
                                        //println!("Matching IP address {:?}", raw);
                                        // lets see what IP address we have
                                        let ip_addr: net::IpAddr = match raw.len() {
                                            4 => {
                                                
                                                    net::IpAddr::V4(<net::Ipv4Addr>::new(
                                                        raw[0], raw[1], raw[2], raw[3],
                                                    ))
                                            },
                                            _ => {
                                                // what IPv? is this ? :)
                                                break;
                                            }
                                        };
                                        //println!("Found ID: {}", id);
                                        // let ip_addr = net::IpAddr::from(raw);
                                        match out.get_mut(&id) {
                                            Some(u) => {
                                                u.ip = Some(ip_addr);
                                            }
                                            None => {
                                                out.insert(
                                                    id,
                                                    super::LANDevice::new(
                                                        id,
                                                        None,
                                                        Some(ip_addr),
                                                        None,
                                                    ),
                                                );
                                            }
                                        }
                                        //println!("ROID:{:?} MAC: {:x?}", _table_oid, raw);
                                    }
                                    _ => {
                                        // println!(
                                        //     "ROID:{:?} VALUE(UNKN): {}",
                                        //     _table_oid,
                                        //     String::from_utf8_lossy(raw)
                                        // );
                                    }
                                }
                            }
                            // if let Value::IpAddress(raw) = _name {
                            //     match _table_oid[0] {
                            //         4 => {}
                            //         _ => {}
                            //     }
                            //     //println!("ROID:{:?} VALUE: {:x?}", _table_oid, _name);
                            // }

                        } else {
                            println!("OID/ROID mismatch");
                            done = true;
                        }
                    }
                    //Err(SnmpError::UnsupportedVersion)
                }
                //Err(e) => Err(e),
                Err(_) => {}
            }
        }
    }

    //}
    //Err(SnmpError::UnsupportedVersion)
    let mut output: Vec<super::LANDevice> = Vec::new();
    let _hash_to_vec: Vec<_> = out.into_iter().map(|v| output.push(v.1)).collect();
    Ok(output)
    //}

    //unimplemented!()
}
